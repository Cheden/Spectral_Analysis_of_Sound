# The Spectral Analysis of Sound

The main thing used in this program is the fourier transform. The fourier transform is a mathematical function that's used to decompose waveforms into the varying sinusoidal waves that make it up. It's essentially the mathematical equivalent of a prism, but for sound. Below is the equation, but it's not necessary to completely understand the equation in order for you to understand the project.

𝑇(𝑡)=8𝜋2∑𝑘=1∞(−1)𝑘𝑛𝑠𝑖𝑛(2𝜋(2𝑘−1)𝑓𝑡)2𝑘−1

## Installation
In order to run and plot the fourier transform youll need to install the following packages: Numpy, Scipy, and matplotlib. This can be done using the following command. 

```bash
sudo apt-get install python3-numpy python3-scipy python3-matplotlib
```

## Gathering Data
Before even using the fourier transform youll need to gather data. You can use voice memos, or the android/samsung equivalent to record audio samples with. You can also just use your computer microphone. Then convert the files you recorded into wav. 

In order for the code to work, youll need to insert the right file. Make sure to import the right file using:
```bash
path = 'path/filename.wav'	
samplerate, data = wavfile.read(path)
```


## Results
After using matplotlibs zooming function to find the first peak, you can run it into the code to determine whether it's harmonic or inharmonic. 

The waveform represent all the various sin waves that make up the sound. The x axis represents its intensity, or volume, and the y axis represents the frequency. frequency being how many times the air oscillates per second. 
