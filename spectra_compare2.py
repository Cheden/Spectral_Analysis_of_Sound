#! /usr/bin/env python3


#importing the proper packages
import numpy as np
import scipy
from scipy.io import wavfile
import matplotlib.pyplot as plt
from scipy.fft import fft, fftfreq

def sin_wave(f):
	num_samples = 44100 #audio sampling rate 
	t = [i*1/(num_samples - 1) for i in range(num_samples)]
	sin_wave = [np.sin(2*np.pi*f*j) for j in t]

#find the files 
path = 'wav_files/waterbottle_mid.wav'

#run the file through a FFT
def spectra(path):
    samplerate, data = wavfile.read(path)
    N = len(data) #number of samples 
    T = 1/samplerate #the rate per scond
    x = np.linspace(0, N*T, N, endpoint=False)
    y = np.array(data)[:,0]
    yf = fft(y)
    xf = fftfreq(N, T)[:N//2]
    yff = np.log(2.0/N * np.abs(yf[0:N//2]))
    return [yff, xf]

middle_path='wav_files/trumpet_a.wav'
top_path='wav_files/ukulele_a.wav'


[yf_top, xf_top] = spectra(top_path)
[yf_middle, xf_middle] = spectra(middle_path)

fig, axs = plt.subplots(2, 1)

axs[0].plot(xf_top, yf_top, 'k')
axs[1].plot(xf_middle, yf_middle, 'k')

for i in range(1, 50):
        axs[0].vlines(441.3*i, -8, 8, colors='red', lw=0.8)
        axs[0].annotate(str(441.3*i)[:6], (441.3*i+15, 7.5))
        axs[1].vlines(395.2*i, -8, 8, colors='red', lw=0.8)
        axs[1].annotate(str(395.2*i)[:6], (395.2*i+15, 7.5))

#plt.plot(xf, yff, color='k', lw=1)
 
#plots the xy axis and title
for i in range(3):
    axs[1].set_xlabel('Frequency (Hz)',fontsize=17)
    axs[0].set_ylabel('Ukulele', fontsize=15)
    axs[1].set_ylabel('Trumpet', fontsize=15)

#sets range of graph
axs[0].set_xlim((0,3000))
axs[1].set_xlim((0,3000))

#shows the graph 
plt.show()
