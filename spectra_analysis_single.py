#! /usr/bin/env python3


#importing the proper packages
import numpy as np
import scipy
from scipy.io import wavfile
import matplotlib.pyplot as plt
from scipy.fft import fft, fftfreq

#find the files 
path = 'wav_files/crick.wav'
samplerate, data = wavfile.read(path)

#run the file through a FFT 
N = len(data) #number of samples 
T = 1/samplerate #the rate per scond
x = np.linspace(0, N*T, N, endpoint=False)
y = np.array(data)[:,0]
yf = fft(y)
xf = fftfreq(N, T)[:N//2]
yff = np.log(2.0/N * np.abs(yf[0:N//2]))

'''for i in range(1, 50):
        fundy = 441.3
        plt.vlines(fundy*i, -8, 8, colors='red', lw=0.8)
        plt.annotate(str(fundy*i)[:6], (fundy*i+15, 7.5))
'''
plt.plot(xf, yff, color='k', lw='1')

#plots the xy axis and title
plt.xlabel('Frequency (Hz)', fontsize=18)
plt.ylabel('Intensity (dB)', fontsize=18)
plt.title(path.split('/')[-1], fontsize=19)

#plt.gca().set_xlim((0,3000))

#shows the graph 
plt.show()
