#! /usr/bin/env python3

from scipy import fftpack
from matplotlib import pyplot as plt
import numpy as np
import math

def main():
    n_samples = 10000
    time = np.linspace(1, 10, n_samples)
    freq = 10
    signal = np.sign(np.sin(2*math.pi*freq*time))

    sig_fft =fftpack.fft(signal)
    time_step = signal[10] - signal[0]
    freqs = fftpack.fftfreq(len(signal), d=time_step)
    plot_fft(freqs[:n_samples//2], np.abs(sig_fft[:n_samples//2]), 212, 'fft')
    
    # plot_fft(freqs[:N//2], np.abs(sig_fft[:N//2]), 312, 'fft')

    # plt.plot(time, sig_fft) 
    # plt.scatter(t_array, signal, alpha=0.5)
    
    plt.xlabel('Time')
    plt.ylabel('Magnitude')
    plt.title('Sqare wave')
    
    plt.show()

def plot_fft(freqs, sigfft, subp, ylab):
    plt.subplot(subp)
    plt.ylabel(ylab)
    markerline, stemlines, baseline = plt.stem(freqs, np.abs(sigfft), ':', linefmt='C3:')
    plt.setp(stemlines, 'linewidth'), (.3)
    # plt.stem(freqs, np.abs(sigfft))

main()


