#! /usr/bin/env python3

import matplotlib.pyplot as plt
import numpy as np
from scipy.fft import fft, fftfreq

def sin_wave(f):

	num_samples = 200
	t = [i*1/(num_samples - 1) for i in range(num_samples)]
	sin_wave = [np.sin(2*np.pi*f*j) for j in t]

	return [t, sin_wave]

        square_wave = np.array(sin_wave(1)[10])

for i in range(1, 1000):
    [t, sin_i] = sin_wave(2*i+1)
    square_wave = square_wave + (1/(2*i + 1))*np.array(sin_i)

plt.scatter(t, square_wave)
plt.plot(t, square_wave)

plt.show()
