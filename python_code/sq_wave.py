#! /usr/bin/env python3

"""
Objective is to create a square wave by adding sine waves together.
"""

"""
import packages
"""
import matplotlib.pyplot as plt
import numpy as np
from scipy.fft import fft, fftfreq

"""
graphing a sine wave of frequency 3
y = sin(2*pi*3*t)
"""

# frequency = 3
# num_samples = 100

# t = [i*1/(num_samples - 1) for i in range(num_samples)]
# sin_wave = [np.sin(2*np.pi*frequency*j) for j in t]

# plt.scatter(t, sin_wave)
# plt.plot(t, sin_wave)

# plt.show()


"""
Adding sine waves by "hand" to create a square wave. 
"""

def sin_wave(f):
	"""Takes a frequency f, and returns a list of y-values for the sine wave of frequency f. 
	The sine wave is defined over time from 0s to 1s. 
	"""
	num_samples = 100
	t = [i*1/(num_samples - 1) for i in range(num_samples)]
	sin_wave = [np.sin(2*np.pi*f*j) for j in t]

	return [t, sin_wave]

[t, sin1] = sin_wave(1)
[t, sin3] = sin_wave(3)
[t, sin5] = sin_wave(5)
[t, sin7] = sin_wave(7)
[t, sin9] = sin_wave(9)
[t, sin11] = sin_wave(11)
[t, sin13] = sin_wave(13)
[t, sin15] = sin_wave(15)

square_wave = (1/1)*np.array(sin1) + (1/3)*np.array(sin3) + (1/5)*np.array(sin5) + (1/7)*np.array(sin7) + (1/9)*np.array(sin9) + (1/11)*np.array(sin11) + (1/13)*np.array(sin13) + (1/15)*np.array(sin15) 

N = 100                                  #number of samples
T = 1.0 / N                                     #sample spacing
x = np.linspace(0, N*T, N, endpoint=False)
y = square_wave
yf = fft(y)
xf = fftfreq(N, T)[:N//2]

plt.xlabel('Time')
plt.ylabel('Magnitude')
plt.title('Square wave')

#plt.scatter(t, square_wave)
#plt.plot(t, square_wave)

plt.plot(xf, 2.0/N * np.abs(yf[0:N//2]))
plt.scatter(xf, 2.0/N * np.abs(yf[0:N//2]))

plt.show()

"""
Adding sine waves with a for loop.
"""

#square_wave = np.array(sin_wave(1)[1])

#for i in range(1, 1000):
#       [t, sin_i] = sin_wave(2*i+1)
#      square_wave = square_wave + (1/(2*i + 1))*np.array(sin_i)

#plt.scatter(t, square_wave)
# plt.plot(t, square_wave)

#plt.show()







