#! /usr/bin/env python3

import scipy
import peakutils 
from scipy.io import wavfile
from scipy.signal import find_peaks
import matplotlib.pyplot as plt
import numpy as np
from scipy.fft import fft, fftfreq

def sin_wave(f):
	num_samples = 44100   
	t = [i*1/(num_samples - 1) for i in range(num_samples)]
	sin_wave = [np.sin(2*np.pi*f*j) for j in t]

samplerate, data = wavfile.read('wav_files/trumpet_a.wav')

#number of samples in the sound file
N = len(data)
#the rate per scond the microphone is taking samples
T = 1/samplerate
x = np.linspace(0, N*T, N, endpoint=False)
y = np.array(data)[:,0]
yf = fft(y)
xf = fftfreq(N, T)[:N//2]
yff = 2.0/N * np.abs(yf[0:N//2])

plt.xlabel('Frequency')
plt.ylabel('Magnitude')
plt.title('Spectra')

plt.plot(xf, yff, color='k')

plt.show()

